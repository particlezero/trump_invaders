# README #

PURPOSE:  
Trump Invaders started as a project to learn how to use the pygame module
for python, animation, particle effects and further study into the use of classes.
Eventually it turned into the shameful entity you see before you.

The code is pretty messy, likely still with some redundant or artifact code (I cleaned
up most of it.)  This project began with a simple tutorial.  I added functionality
little by little as I learned more about pygame to produce animated and particle
effects (particle effects almost direct from stackoverflow).

Trump Invaders is coded in python.  Requires pygame module installed.

All graphics were generated and/or edited by the author.


USAGE: 
1. Have python 2.7 installed, along with pygame
2. in game directory: python trump_invaders.py
3. submit to your new overlord


TO_DO:

* clean up that code!
* recode enemy list
* sound
* multiple shot capability (hero and baddie)
* "next level" interim screen
* high scores
