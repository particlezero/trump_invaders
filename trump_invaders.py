import sys
import pygame
import math
from pygame import *
import random
from vMenu import *

DISPLAY_WIDTH = 800
DISPLAY_HEIGHT = 600
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 128)
ORANGE = (255, 150, 0)
RED = (255, 80, 80)

class Particle: # particle class for nifty 'splosions
    def __init__(self,position,color):
        self.x = position[0]
        self.y = position[1]
        self.velocity_x = random.uniform(-10, 10)
        self.velocity_y = random.uniform(-10, 10)
        self.gravity = .5
        self.lifespan = 50
        self.age = 0
        self.color = color
        self.circle_radius = random.randint(2, 3)
    def fireworks_update(self):
        self.x = self.x + self.velocity_x
        self.y = self.y + self.velocity_y
        self.velocity_y = self.velocity_y + self.gravity
        self.age += 1
    def starfield_update(self):
        self.x = self.x + self.velocity_x
        self.y = self.y + self.velocity_y
    def draw_particles(self):
        pygame.draw.circle(screen, self.color, (int(self.x), int(self.y)), self.circle_radius)
    def dead(self):
        if self.age >= self.lifespan:
            return True
        elif not 0 <= self.x <= DISPLAY_WIDTH:
            return True
        elif not 0 <= self.y <= DISPLAY_HEIGHT:
            return True
        else:
            return False

class k(pygame.sprite.Sprite):  # Sprite class for player, enemies
    def __init__ (self, xpos, ypos, filename1, filename2):
        super(k, self).__init__()
        self.x = xpos
        self.y = ypos

        self.index = 0
        self.bitmaps = []
        self.bitmaps.append(load_image(filename1))
        self.bitmaps.append(load_image(filename2))
        self.bitmap = self.bitmaps[self.index]
        self.bitmap.set_colorkey(BLACK)

        self.animation_frames = 40
        self.current_frame = 0

    def set_position(self, xpos, ypos):
        self.x = xpos
        self.y = ypos

    def render(self):
        screen.blit(self.bitmap, (self.x, self.y))

    def update(self):
        self.current_frame += 1
        if self.current_frame >= self.animation_frames:
            self.current_frame = 0
            self.index += 1
            if self.index > 1:
                self.index = 0
            self.bitmap = self.bitmaps[self.index]
            self.bitmap.set_colorkey(BLACK)

class fire(pygame.sprite.Sprite):  # Sprite class for weapons fire
    def __init__(self, xpos, ypos, filename1, filename2, filename3, filename4):
        super(fire, self).__init__()
        self.x = xpos
        self.y = ypos

        self.index = 0
        self.bitmaps = []
        self.bitmaps.append(load_image(filename1))
        self.bitmaps.append(load_image(filename2))
        self.bitmaps.append(load_image(filename3))
        self.bitmaps.append(load_image(filename4))
        self.bitmap = self.bitmaps[self.index]
        self.bitmap.set_colorkey(BLACK)

        self.animation_frames = 5
        self.current_frame = 0

    def set_position(self, xpos, ypos):
        self.x = xpos
        self.y = ypos

    def render(self):
        screen.blit(self.bitmap, (self.x, self.y))

    def update(self):
        self.current_frame += 1
        if self.current_frame >= self.animation_frames:
            self.current_frame = 0
            self.index += 1
            if self.index > 3:
                self.index = 0
            self.bitmap = self.bitmaps[self.index]
            self.bitmap.set_colorkey(BLACK)


def quit_game():
    sys.exit()

pause = False

def game_pause():   # Pause screen
    pause = True
    lT = pygame.font.Font("/usr/share/fonts/truetype/droid/DroidSans-Bold.ttf", 96)
    lS = lT.render("Game Paused", 1, GREEN)
    lR = lS.get_rect()
    lR.center = ((DISPLAY_WIDTH/2), (DISPLAY_HEIGHT/2))
    iL = ["-p- to continue", "-m- for main menu", "-q- to quit"]
    d = 1
    for i in iL:
        lA = pygame.font.Font("freesansbold.ttf", 48)
        lB = lA.render(i, 1, WHITE)
        lC = lB.get_rect()
        lC.center = ((DISPLAY_WIDTH/2), (int(DISPLAY_HEIGHT/2)+(d*50)))
        d += 1
        screen.blit(lB, lC)
    screen.blit(lS, lR)

    while pause == True:
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == KEYUP:
                if event.key == K_p:
                    pause = False
                if event.key == K_m:
                    quit = 1
                    g.run()
                if event.key == K_q:
                    sys.exit()

        pygame.display.update()
        clock.tick(15)

def game_over(stat_dictionary):  # Game Over screen
    display.set_caption("Trump Invaders - You Suck")
    screen.fill((0,0,0))
    go = load_image('data/gameoverl.bmp')
    goR = go.get_rect()
    goh = goR.height
    goR.center = ((DISPLAY_WIDTH/2), ((DISPLAY_HEIGHT/2)-(goR.height/3)))
    screen.blit(go, goR)
    sfont = pygame.font.Font("/usr/share/fonts/truetype/droid/DroidSans.ttf", 32)
    smsg = sfont.render("Your Score: " + str(stat_dictionary['Score']), 1, WHITE)
    srect = smsg.get_rect()
    srect.center = ((DISPLAY_WIDTH/2), (DISPLAY_HEIGHT/2)+goR.height/3)
    screen.blit(smsg, srect)
    lmsg = sfont.render("Your Level: " + str(stat_dictionary['Level']), 1, WHITE)
    lrect = lmsg.get_rect()
    lrect.center = ((DISPLAY_WIDTH/2), (DISPLAY_HEIGHT/2)+(goR.height/3) + srect.height + 20)
    screen.blit(lmsg, lrect)
    go = True

    while go:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == MOUSEBUTTONDOWN:
                g.run()
        display.update()
        clock.tick(15)

def Intersect(s1_x, s1_y, s2_x, s2_y):
    if (s1_x > s2_x -32) and (s1_x < s2_x +32) and (s1_y > s2_y - 32) and (s1_y < s2_y + 32):
        return 1
    else:
        return 0

def load_image(name):
    image = pygame.image.load(name)
    return image

init()

backdrop = image.load('data/starfield.bmp')
backdrop = transform.scale(backdrop, (DISPLAY_WIDTH, DISPLAY_HEIGHT))
clock = pygame.time.Clock()

def game_run(lives = 3, score = 0, level = 1):
    extralives = lives
    done = 0
    round = 0
    currentscore = score
    gamelevel = level
    enemies = []
    egroup = sprite.Group()
    wgroup = sprite.Group()
    enemyspeed = level
    display.set_caption('Trump Invaders')
    ufo_time = 0
    ufo_presence = False
    ufo_speed = level
    particles = []

    x = 0
    for count in range(10):
        enemies.append(k(50 * x + 50, 50, 'data/baddie_1.bmp', 'data/baddie_2.bmp'))
        x += 1

    x = 0
    for count in range(10):
        enemies.append(k(50 * x + 50, 100, 'data/baddie_4.bmp', 'data/baddie_3.bmp'))
        x += 1

    x = 0
    for count in range(10):
        enemies.append(k(50 * x + 50, 150, 'data/baddie_1.bmp', 'data/baddie_2.bmp'))
        x += 1

    for x in enemies:
        egroup.add(x)

    heroheight = DISPLAY_HEIGHT - 80
    hero = k(20, heroheight, 'data/ship1.bmp', 'data/ship2.bmp')

    enemymissile = fire(0, DISPLAY_HEIGHT, 'data/enemy_fire1.bmp', 'data/enemy_fire2.bmp', 'data/enemy_fire3.bmp', 'data/enemy_fire4.bmp')
    shot = fire(0, DISPLAY_HEIGHT, 'data/player_fire1.bmp', 'data/player_fire2.bmp', 'data/player_fire3.bmp', 'data/player_fire4.bmp')
    wgroup.add(shot)
    wgroup.add(enemymissile)
    ufos = []
    ugroup = sprite.Group()

    ''' Main game loop '''
    while done == 0:
        statd = {"Score": currentscore, "Lives": extralives, "Level": gamelevel}

        if extralives == 0:
            done = 1
            game_over(statd)
        screen.blit(backdrop, (0, 0))

        # UFO stuff
        if ufo_presence == True:
            if ufo.x >= DISPLAY_WIDTH:
                ufo.kill()
                del ufo
                ufo_presence = False
            elif Intersect(ufo.x, ufo.y, shot.x, shot.y):
                currentscore += 100
                particles.extend([Particle((ufo.x, ufo.y), ORANGE) for count in range(30)])
                
                del ufo
                ufo_presence = False
                shot.y = DISPLAY_HEIGHT
            else:
                ufo.x += ufo_speed
                ufo.render()
        elif ufo_presence == False and ufo_time > 3000:
            ufos.append(k(20, 5, 'data/ufo1.bmp', 'data/ufo2.bmp'))
            ufo = ufos[len(ufos)-1]
            ugroup.add(ufo)
            ufo_presence = True
            ufo.render()
            ufo_time = 0

        else:
            pass

        for count in range(0, len(enemies)):
            enemies[count].x += enemyspeed

            if enemies[count].x > (DISPLAY_WIDTH - 10):
                enemyspeed = abs(enemyspeed)*-1
                for c in range(len(enemies)):
                    enemies[c].y += 5

            if enemies[count].x < 10:
                enemyspeed = abs(enemyspeed)
                for c in range(len(enemies)):
                    enemies[c].y += 5
            enemies[count].render()

        if shot.y < DISPLAY_HEIGHT and shot.y > 0:
             shot.y += -5
             shot.render()

        if enemymissile.y >= DISPLAY_HEIGHT and len(enemies) > 0:
             enemymissile.x = enemies[random.randint(0, len(enemies)/3)].x
             enemymissile.y = enemies[random.randint(0, len(enemies)-1)].y + 5
        if Intersect(hero.x, hero.y, enemymissile.x, enemymissile.y):
             enemymissile.x = enemies[random.randint(0, len(enemies)/3)].x
             enemymissile.y = enemies[random.randint(0, len(enemies)-1)].y + 5
             enemymissile.y = enemies[random.randint(0, 3)].y
             extralives -= 1
             hero.x = 20
             hero.render()

        for i in range(0, len(enemies)):
            if Intersect(shot.x, shot.y, enemies[i].x, enemies[i].y):
                particles.extend([Particle((enemies[i].x, enemies[i].y), RED) for count in range(30)])
                del enemies[i]
                currentscore += 10
                shot.y = DISPLAY_HEIGHT
                break

        if len(enemies) == 0:
            gamelevel+=1
            game_run(lives = extralives, score = currentscore, level = gamelevel)

        for ourevent in event.get():
            if ourevent.type == QUIT:
                quit = 1
            if ourevent.type == KEYDOWN:
                if ourevent.key == K_RIGHT and hero.x < DISPLAY_WIDTH-10:
                    hero.x += 5
                if ourevent.key == K_LEFT and hero.x > 10:
                    hero.x -= 5
                if ourevent.key == K_SPACE:
                    shot.x = hero.x
                    shot.y = hero.y
            if ourevent.type == KEYUP:
                if ourevent.key == K_p:
                    pause = True
                    game_pause()

        for lp in particles:
            lp.fireworks_update()
            lp.draw_particles()
            if lp.dead():
                particles.remove(lp)

        enemymissile.render()
        enemymissile.y += 5
        hero.update()
        wgroup.update()
        egroup.update()
        ugroup.update()
        kv = ''
        progimg = statd['Lives']
        imgfile = 'data/prog'+str(progimg)+'.bmp'
        kv += "Level: " +str(statd['Level'])+"      "
        kv += "Score: "
        kv += str(statd['Score'])+"      "
        kv += "Your Civil Liberties: "
        bannerfont = pygame.font.Font("freesansbold.ttf", 18)
        banner_color = (255, 255, 255)
        banner = bannerfont.render(kv, 1, banner_color)
        bannerrect = banner.get_rect()
        banner_width = bannerrect.width
        banner_height = bannerrect.height
        img = load_image(imgfile)
        imgr = img.get_rect()
        imgw = imgr.width
        imgr.center = (DISPLAY_WIDTH-20-(imgw/2), (DISPLAY_HEIGHT - banner_height))
        bannerrect.center = (DISPLAY_WIDTH-30-imgw-(banner_width/2), (DISPLAY_HEIGHT - banner_height))
        screen.blit(img, imgr)
        screen.blit(banner, bannerrect)
        hero.render()
        round += 1
        display.update()
        ufo_time += 1
        time.delay(5)
    else:
        g.run()
    clock.tick(60)
if __name__ == "__main__":
    screen = display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT), 0, 32)
    key.set_repeat(1, 1)
    display.set_caption('Trump Invaders - Game Menu')
    funcs = {'Start': game_run,
             'Quit': sys.exit}
    menu_items = ('Start', 'Quit')
    g = GameMenu(screen, funcs.keys(), funcs)
    g.run()    
